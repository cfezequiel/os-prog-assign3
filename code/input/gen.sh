#! /bin/sh

# Create input files needed by fs_test
# 1MB file
dd if=/dev/zero of=file_1mb.bin count=1024 bs=1024

# 10MB file
dd if=/dev/zero of=file_10mb.bin count=10240 bs=1024

# 1GB file
dd if=/dev/zero of=file_1gb.bin count=1048576 bs=1024

