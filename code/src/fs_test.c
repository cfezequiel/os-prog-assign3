/**
 * /file fs_test.c
 *
 * Run tests to examine characteristics of file system.
 */
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include "cycle.h"

#define REPS 32
#define MAX_STR_LEN 100
#define CPU_SPEED_MHZ 2666.680
#define CPU_SPEED_HZ 2666680000ul

int file_exists(char *filename)
{
    struct stat buf;
    int i = stat (filename, &buf);
     /* File found */
     if ( i == 0 )
     {
       return 1;
     }
     return 0;
}

inline void check_write(int fd, const void *buf, size_t count)
{
    int bytes;

    bytes = write(fd, buf, count);
    if (bytes < 0)
    {
        printf("Error writing to file: %s\n", strerror(errno));
        exit(-1);
    }
}

inline int check_open(char *filename, int flags)
{
    int fd;

    fd = open(filename, flags);
    if (fd < 0)
    {
        printf("Error opening file %s: %s\n", filename, strerror(errno)); 
        exit(-1);
    }

    return fd;
}

inline int check_lseek(int fd, int offset, int whence)
{
    int fpos;

    fpos = lseek(fd, offset, whence);
    if (fpos < 0)
    {
        printf("Error seeking file position: %d\n", errno);
        exit(-1);
    }
    return fpos;
}

inline void check_close(int fd)
{
    int res = close(fd);
    if (res < 0)
    {
        printf("Error closing file %d: %s\n", fd, strerror(errno));
        exit(-1);
    }
}

double gettime_read(int fd, size_t bytes, double *elapsed_time)
{
    char *buf;
    int bytes_read;
    ticks t0, t1;

    assert(fd > 0);

    /* Allocate space for buffer. */
    buf = malloc(sizeof(char) * bytes);
    if (buf == NULL)
    {
        printf("Error allocating buffer space.\n");
        return -1;
    }

    /* Read bytes from file and get start, end times */
    t0 = getticks();
    bytes_read = read(fd, (void *) buf, bytes);
    t1 = getticks();
    if (bytes_read < 0)
    {
        printf("Error reading block: %s\n", strerror(errno));
        return -1;
    }
    *elapsed_time = elapsed(t1, t0) / CPU_SPEED_HZ;

    /* Free allocated space for buffer */
    free(buf);

    return bytes_read;
}

int test_multiple_block_sizes(char *infile, char *outdir, int bmin, int bmax, int step)
{
    FILE *fp;
    int fd;
    int i;
    int j;
    int block_count;
    double elapsed_time;
    double acc_time = 0;
    double acc_acc_time = 0;
    double ave_time;
    double ave_ave_time;
    char outfile[MAX_STR_LEN];
    int bytes_read;

    snprintf(outfile, MAX_STR_LEN, "%s/test_multiple_block_sizes_%d_%d_%d.csv", 
            outdir, bmin, bmax, step);
    fp = fopen(outfile, "w");

    fprintf(fp, "Chunk size, Elapsed time\n");
    for (i = bmin; i < bmax; i+=step)
    {
        for (j = 0; j < REPS; j++)
        {
            acc_time = 0;
            block_count = 0;
            fd = check_open(infile, O_RDONLY | O_SYNC);
            do
            {
                bytes_read = gettime_read(fd, i, &elapsed_time);
                if (bytes_read < 0)
                {
                    return 1;
                }
                acc_time += elapsed_time;
                block_count++;
            } while (bytes_read > 0);

            ave_time = acc_time / block_count;
            check_close(fd);
            acc_acc_time += ave_time;
        }
        ave_ave_time = acc_acc_time / REPS;
        fprintf(fp, "%d, %f\n", i, ave_ave_time);
    }

    fclose(fp);

    return 0;
}

/**
 * Get elapsed times (seconds) for various reads of a file 
 */
int read_chunks(char *infile, int chunk_size, int reps)
{
    struct stat s;
    int fd;
    int block_count;
    int file_size;
    int bytes_read;
    int i;
    int j;
    double *out_buf;
    double elapsed_time;
    double ave_time;
    char *buf;
    char outfile[MAX_STR_LEN];
    ticks t0, t1;
    FILE *fp;

    assert(infile != NULL);

    /* Open input file */
    fd = check_open(infile, O_RDONLY | O_SYNC);

    /* Get file size in bytes*/ 
    fstat(fd, &s);
    file_size = s.st_size;

    /* If output file exists, quit */
    snprintf(outfile, MAX_STR_LEN, "output/read_chunks_%d_%d_%d.csv",
             file_size, chunk_size, reps);
    if (file_exists(outfile))
    {
        return 0;
    }

    /* Get number of blocks */
    block_count = file_size / chunk_size;

    /* Initialize buffers */
    buf = malloc(sizeof(char) * chunk_size);
    if (buf == NULL)
    {
        printf("Error: could not allocate read buffer.\n");
        return 1;
    }
    out_buf = calloc(block_count, sizeof(double));
    if (out_buf == NULL)
    {
        printf("Error: could not allocate output buffer.\n");
        return 1;
    }

    /* Close file */
    check_close(fd);

    for (i = 0; i < reps; i++)
    {
        fd = check_open(infile, O_RDONLY | O_SYNC);
        for (j = 0; j < block_count; j++)
        {
            t0 = getticks();
            bytes_read = read(fd, buf, chunk_size);
            t1 = getticks();
            elapsed_time = elapsed(t1, t0);
            if (bytes_read < 0)
            {
                printf("Error reading from file: %s\n", strerror(errno));
                return 1;
            }
            out_buf[j] += elapsed_time;
        }
        check_close(fd);
    }

    /* Open output file */
    fp = fopen(outfile, "w");
    if (fp == NULL)
    {
        printf("Could not open output file %s.\n", outfile);
        return 1;
    }

    
    fprintf(fp, "Block no., Elapsed time\n");
    for (j = 0; j < block_count; j++)
    {
        ave_time = (out_buf[j] / (double) reps) / (double) CPU_SPEED_HZ;
        fprintf(fp, "%d, %f\n", j, ave_time);
    }

    /* Close output file */
    fclose(fp);

    /* Free allocated memory */
    free(out_buf);
    free(buf);

    return 0;
}

int write_chunks(char *file, int block_size, int numblocks)
{
    char outfile[MAX_STR_LEN];
    char *buf;
    int fd;
    FILE *fp;
    int i;
    int j;
    int bytes;
    int res = 0;
    int fpos;
    double elapsed_time;
    double acc_time = 0;
    double ave_time;
    ticks t0, t1;

    /* Create buffer for writing */
    buf = malloc(sizeof(char) * numblocks);
    if (buf == NULL)
    {
        printf("Error allocating buffer.\n");
        return 1;
    }

    /* Open output file */
    snprintf(outfile, MAX_STR_LEN, "output/write_chunks_%d_%d.csv", block_size, numblocks);
    fp = fopen(outfile, "w");
    if (fp == NULL)
    {
        return 1;
    }

    /* Open file */
    fd = check_open(file, O_CREAT | O_WRONLY | O_SYNC);

    fprintf(fp, "Block no., Elapsed Time\n");
    for (i = 1; i <= numblocks; i++)
    {
        fpos = check_lseek(fd, 0, SEEK_CUR);
        acc_time = 0;
        for (j = 0; j < REPS; j++)
        {
            elapsed_time = 0;
            /* Write to file */
            t0 = getticks();
            bytes = write(fd, buf, block_size);
            t1 = getticks();
            if (bytes < 0)
            {
                printf("Error writing block: %s\n", strerror(errno));
                return 1;
            }
            elapsed_time += elapsed(t1, t0);

            /* Sync file buffer */
            t0 = getticks();
            res = fsync(fd);
            t1 = getticks();
            elapsed_time += elapsed(t1, t0);
            acc_time += elapsed_time;
            check_lseek(fd, fpos, SEEK_SET);
        }
        check_lseek(fd, bytes, SEEK_CUR);
        ave_time = acc_time / REPS;

        /* Output timestamp */
        fprintf(fp, "%d, %f\n", i, ave_time / CPU_SPEED_HZ);
    }

    /* Close file */
    check_close(fd);

    /* Close output file */
    fclose(fp);

    /* Delete the file */
    res = remove(file);

    /* Free allocated memory */
    free(buf);

    return res;
}

int main(void)
{
    int res = 0;
#if 0
    int chunk_sizes[] = {8, 32, 512, 4096, 8192};
    int steps[] = {1, 8, 32, 64};
    char files[][MAX_STR_LEN] = {"input/file_1mb.bin", "input/file_10mb.bin",
                    "input/file_1gb.bin"};
    int i, j, k;
    
    for (i = 0; i < sizeof(steps)/sizeof(steps[0]); i++)
    {
        for (j = 0; j < sizeof(chunk_sizes)/sizeof(chunk_sizes[0]); j ++)
        {
            for (k = 0; k < sizeof(files)/sizeof(files[0]); k++)
            {
                res += read_chunks(files[k], chunk_sizes[j], steps[i]);
            }
        }
    }
    /* Block size */
    res += read_chunks("input/file_1mb.bin", 8, 32);
    res += read_chunks("input/file_1mb.bin", 32, 32);
    res += read_chunks("input/file_1mb.bin", 128, 32);
    res += read_chunks("input/file_1mb.bin", 512, 1);
    res += read_chunks("input/file_1mb.bin", 512, 8);
    res += read_chunks("input/file_1mb.bin", 512, 32);
    res += read_chunks("input/file_1mb.bin", 4096, 32);
    res += read_chunks("input/file_1mb.bin", 8192, 32);

    /* Prefetch size */
    res += read_chunks("input/file_10mb.bin", 4096, 32);
    res += read_chunks("input/file_10mb.bin", 8192, 32);
    res += read_chunks("input/file_10mb.bin", 12288, 32);


    /* Inode */
    res += write_chunks("tmp.bin", 4096, 13);
    res += write_chunks("tmp.bin", 4096, 20);
    res += write_chunks("tmp.bin", 4096, 32);

    /* File cache */
    res += read_chunks("input/file_10mb.bin", 524288, 2);
    res += read_chunks("input/file_10mb.bin", 1048576, 2);
    res += read_chunks("input/file_1gb.bin", 524288, 2);
    res += read_chunks("input/file_1gb.bin", 1048576, 2);
    res += read_chunks("input/file_1gb.bin", 2 * 1048576, 2);
    res += read_chunks("input/file_1gb.bin", 4 * 1048576, 2);
    res += read_chunks("input/file_1gb.bin", 6 * 1048576, 2);
    res += read_chunks("input/file_1gb.bin", 8 * 1048576, 2);
    res += read_chunks("input/file_10mb.bin", 524288, 2);
    res += read_chunks("input/file_10mb.bin", 1048576, 2);
    res += read_chunks("input/file_10mb.bin", 2 * 1048576, 2);
    res += read_chunks("input/file_10mb.bin", 4 * 1048576, 2);
    res += read_chunks("input/file_10mb.bin", 8 * 1048576, 2);

    res += read_chunks("input/file_10mb.bin", 2 * 4096, 2);
    res += read_chunks("input/file_10mb.bin", 4 * 4096, 2);
    res += read_chunks("input/file_10mb.bin", 8 * 4096, 2);
    res += read_chunks("input/file_10mb.bin", 16 * 4096, 2);
    res += read_chunks("input/file_10mb.bin", 32 * 4096, 2);
    res += read_chunks("input/file_10mb.bin", 64 * 4096, 2);
    res += read_chunks("input/file_10mb.bin", 128 * 4096, 2);
    res += read_chunks("input/file_1gb.bin", 524288, 2);
    res += read_chunks("input/file_1gb.bin", 1 * 1048676, 2);
    res += read_chunks("input/file_1gb.bin", 2 * 1048676, 2);
    res += read_chunks("input/file_1gb.bin", 4 * 1048676, 2);
    res += read_chunks("input/file_1gb.bin", 8 * 1048676, 2);
    res += read_chunks("input/file_1gb.bin", 10 * 1048676, 2);
    res += read_chunks("input/file_1gb.bin", 12 * 1048676, 2);
#else
    /* Used in report */
    /* Block size */
    res += read_chunks("input/file_1mb.bin", 512, 32);
    /* Prefetch size */
    res += read_chunks("input/file_10mb.bin", 4096, 32);
    /* File cache size */
    res += read_chunks("input/file_1gb.bin", 524288, 2);
    /* Number of direct pointers in inode */
    res += write_chunks("tmp.bin", 4096, 20);
#endif

    if (!res)
    {
        printf("All tests successful!\n");
    }
    else
    {
        printf("Tests failed: %d\n", res);
    }

    return res;
}
