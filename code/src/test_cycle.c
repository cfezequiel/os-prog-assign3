/**
 * /file test_cycle.c
 *
 * Test cycle_accurate timer used by fs_test
 */

#include <stdio.h>
#include <unistd.h>

#include "cycle.h"

#define SLEEP_TIME 1
#define CPU_SPEED_MHZ 2666.680
#define CPU_SPEED_HZ (CPU_SPEED_MHZ * 1000000.0)

int main(void)
{
    ticks t1, t2;
    double elapsed_time, real_time;

    t1 = getticks();
    sleep(SLEEP_TIME);
    t2 = getticks();

    elapsed_time = elapsed(t2, t1);

    printf("Elapsed time: %f\n", elapsed_time);
    printf("CPU frequency (Hz): %f\n", CPU_SPEED_HZ);
    real_time = elapsed_time / (double) CPU_SPEED_HZ;
    printf("Real time (s): %f\n", real_time);

    return 0;
}


